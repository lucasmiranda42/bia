import os
import argparse

parser = argparse.ArgumentParser(description='Filtra listas de expresión diferencial por genes de interés y significancia estadística')
parser.add_argument('--file_in', '-f', type=str, help='Archivo de entrada')
parser.add_argument('--alpha', '-a', type=float, help='Cutoff de significancia estadística',default=48)
parser.add_argument('--fold', '-l', type=float, help='Cutoff de significancia biològica',default=48)
parser.add_argument('--genelist', '-g', type=str, help='Lista de genes customizada',default="Human")

args = parser.parse_args()
file_in = args.file_in
alpha = args.alpha
fold = args.fold
gene_list = args.genelist

#Lista de genes de interés
if gene_list == "Human":
    Lista_NFKB = "IL6, IL6ST, IL8, IRAK2, IRF1, CXCL1, CXCL2, CXCL3, PLAUR, CCL20, TLR2, TRAF1, TRAF3, IFIH1, HLA-B, HLA-C, ICAM1, TNFRSF9, LTB, CCL2, CCL5, TAP1, TAP2, TAPBP, CD83, IL23A, PSMB8, PSMB9, IL32, CD47, ITGA2, ITGA5, ITGAM, ITGAV, LAMC2, CLDN1, CFB, C3, GBP1, TNFAIP3, SH2B3, NFKBIE, RGS1, CCL4, CCL3, DUSP2, JUNB, GADD45B, CCND2, CXCL13, NAMPT, CXCL9, HEATR1, GPR183, EBI3, LTA, TNF, BCL2A1, CXCL10, IL12B, ID2, PTGER2, ITGB6, SOD1, INSIG1, SOD2, PBEF1, EBI2, NFKB1, NFKB2, REL, RELA, RELB, CHUK, IKBKB, IKBKG, NLK, NFKBIA, NFKBIB, TNFRSF10B, FASLG, BIRC4, TRAF2, IER3S, IER3L, BIRC2, MCL1S, MCL1L, IL1A, IL1B, IL15, CCR5, TNFRSF11A, TNFSF11, IRF7, CD40, CD40LG, CD48, CSF1, CSF1R, CSF2, CCND1, CCND3, CCNG1, MMP9, MMP11, PLAU, CTSB, CXCR4, CXCL12, ICAM1, VCAM1, SELE, TNC, VEGF, PTGS2, BCL2, BCL2L1, BIRC3, C4orf9, CCL22, CCR7, CD23A, CD44, PRR16, CD69, CFLAR, DUSP1, EGR1, EMR1, HLA-F, IER2, IER3, IL15RA, IL2RA, IL4RA, IRF4, KAI1, KLF10, LITAF, LSP1, MYB, NCF2, NK4, PASK, PCAM1, PRKCD, RAFTLIN, RRAS2, SDC4, SLC2A5, SMAD7, STAT5A, STX4A, TNFRSF5, TNFSF2, TNIP, TPMT, VIM, WTA".split(", ")

else:
    with open(gene_list) as g:
        Lista_NFKB = [line.replace("\n","") for line in g][1:]

print(Lista_NFKB)

#Abrimos el archivo
with open(file_in) as f:
    data = [line.split(",") for line in f]

header = data[0]
data = data[1:]

#Filtramos la lista con los genes de interés
Lista_filtrada = [line for line in data if line[6] in Lista_NFKB]

#Filtramos por significancia estadística
if alpha != 48:
    Lista_filtrada = [line for line in Lista_filtrada if float(line[1])<alpha]

#Filtramos por fold change de expresión
if fold != 48:
    Lista_filtrada = [line for line in Lista_filtrada if float(line[5])>fold or float(line[5])<-fold]

print("Datos originales: "+str(len(data)))
print("Datos tras aplicar filtro: "+str(len(Lista_filtrada)))

with open(file_in[:-4]+"_out.csv", 'w') as x:
    x.write(" ".join(header))
    for line in Lista_filtrada:
        x.write(" ".join(line))
