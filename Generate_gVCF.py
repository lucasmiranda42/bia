from sys import argv
from multiprocessing import Pool
import subprocess, time

script, file_in, cores = argv

with open(file_in) as f:
    bam_list = [line.replace('\n','') for line in f]


def Genotype_gVCF(bams):
    with open('gVCFlog.txt', 'a') as x:

        for i in bams:
            start = time.time()

            p = subprocess.call('gatk --java-options "-Xmx16g" HaplotypeCaller -R /mnt/pipeline/bundle/b37/$
            if p == 0:
                x.write('{} gVCF generated successfully'.format(i))

            else:
                x.write('{} gVCF finished with an error'.format(i))
