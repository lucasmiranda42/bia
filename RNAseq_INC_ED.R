#Primero instalamos los paquetes necesarios
install.packages("dplyr")
install.packages("devtools")
source("https://www.bioconductor.org/biocLite.R")
biocLite(c("alyssafrazee/RSkittleBrewer", "ballgown", "genefilter"))

#Cargamos ahora los paquetes a utilizar
library(ballgown)
library(RSkittleBrewer)
library(genefilter)
library(dplyr)
library(devtools)

#Creamos una variable con el directorio a la carpeta que tiene todas las salidas de StringTie
directory = "C://Users/l_vey/Documents/Estudio y carrera de investigación/Labo/Colaboraciones/Juan_Nicola/RNAseq/RNAseq_data/"

#Posteriormente, creamos un ballgown object. Es importante que la estructura de las carpetas conteniendo
#los datos se mantenga: Una carpeta root con una subcarpeta por cada sample a analizar, conteniendo solo
#los archivos .ctab correspondientes (tienen que ser 5).
gc()
#bg = ballgown(dataDir=directory, samplePattern='ERR', meas='all')

#Guardamos el objeto ballgown en un archivo
#save(bg, file='bg.rda')

#Cargamos el objeto de ballgown previamente creado
#load('bg.rda')

#Anotamos el tipo de tumor al cual pertenece cada experimento
pData(bg) = data.frame(id=sampleNames(bg), group=c(rep("FA",44),rep("FTC",49),rep("FV",59),rep("PTC",110)))

#Lo siguiente es filtrar los transcriptos con baja varianza. Quedándonos sólo con aquellos genes que varían más del
#cutoff. rowVars es una función del paquete genefilter que calcula la varianza por fila
bg_filt_FA_PTC = subset(bg,"rowVars(texpr(bg)) >1",genomesubset=TRUE)

#Filtramos por comparación
FA_PTC = subset(bg_filt,"group=='FA'|group=='PTC'",genomesubset=FALSE)
FA_FV = subset(bg_filt,"group=='FA'|group=='FV'",genomesubset=FALSE)
FA_FTC = subset(bg_filt,"group=='FA'|group=='FTC'",genomesubset=FALSE)
FV_PTC = subset(bg_filt,"group=='FV'|group=='PTC'",genomesubset=FALSE)
FV_FTC = subset(bg_filt,"group=='FV'|group=='FTC'",genomesubset=FALSE)
PTC_FTC = subset(bg_filt,"group=='PTC'|group=='FTC'",genomesubset=FALSE)


#Corremos entonces el análisis de expresión diferencial
FA_PTC_results = stattest(FA_PTC, feature='transcript', meas='FPKM', covariate='group', getFC = TRUE)
head(stat_results)
#Filtramos y ordenamos los resultados
FA_PTC_results = FA_PTC_results[which(FA_PTC_results[,"qval"]<0.05),]

o = order(FA_PTC_results[,"qval"], decreasing = FALSE)
FA_PTC_results = FA_PTC_results[o,c("feature","id","fc","pval","qval")]
