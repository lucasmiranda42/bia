#Primero instalamos los paquetes relevantes (puede tardar un ratito)
source("https://bioconductor.org/biocLite.R")
biocLite(c("affy", "limma","simpleaffy","gcrma"))

#Una vez instalados, los cargamos en la sesión
library(affy)
library(simpleaffy)
library(gcrma)
library(limma)
library(ggplot2)

#Comenzamos entonces por abrir los archivos .CEL a utilizar con el paquete affy
#Especificamos el directorio que tiene los archivos .CEL a analizar
celpath = 'C:\\Users\\l_vey\\Documents\\Estudio\\Labo\\Colaboraciones\\Juan_Nicola\\Arrays\\Parte_IV_bis(DAVID)\\Bundle_Hot\\'

#Seteamos el directorio de trabajo
setwd(celpath)

#Luego cargamos todos los .CEL en el directorio
data = ReadAffy(celfile.path=celpath)

#Esto último carga un objeto con todos los .CEL especificados. Pueden verse los
#datos referentes de cada archivo con los comandos
data #Muestra estad�?sticas de todo el batch
data[,2] #Muestra estad�?sticas del segundo archivo

#Si quiero ver las intensidades de alguno de los archivos, uso el comando
int = intensity(data)
int

#Para ver el nombre del archivo .CDF asociado con los datos (qué chip se usó)
cdfName(data)

"Pasamos al control de calidad de los datos"

#Primero vamos a etiquetar los archivos según a qué grupo pertenecen
ph = data@phenoData
ph@data[] = c(rep("HOT",30))
ph

#Como primera medida de control de calidad, vamos a generar histogramas intensidad para todos
#los arrays. Si se ven corrimientos en forma o centro de masa, se va a requerir normalización.
pmexp = pm(data)

sampleNames = vector()
logs = vector()
for (i in 1:14)
{
  sampleNames = c(sampleNames,rep(ph@data[i,1],dim(pmexp)[1]))
  logs = c(logs,log2(pmexp[,i]))
}

logData = data.frame(logInt=logs,sampleName=sampleNames)

dataHist2 = ggplot(logData, aes(logInt, colour = sampleName)) 
dataHist2 + geom_density()

#Graficando box plots en lugar de histogramas
dataBox = ggplot(logData,aes(sampleName,logInt))
dataBox + geom_boxplot()

#Crear MA plots
for (i in 1:14)
{
  name = paste("MAplot",i,".jpg",sep="")
  jpeg(name)
  MAplot(data,which=i)
  dev.off()
}

#Calcular calidad del batch object
data_qc = qc(data)
data_qc

#Del objeto que acabamos de crear podemos extraer la intensidad promedio del background para
#cada array. Estos valores deber�?an ser comparables entre si. Vemos que son similares para los
#FTA, pero que difieren un montón con los de las tiroides normales.
avbg(data_qc)

#Luego podemos obtener los scale factors usados para igualar las intensidades medias de los
#arrays. T�?picamente ninguno deber�?a ser más del triple que otro. Vemos que esto se cumple :)
sfs(data_qc)

#Lo siguiente es mirar qué porcentaje de spots en un array dan una intensidad significativamente
#más grande que la señal.
percent.present(data_qc)

#Por último, los arrays tienen spots especiales de control de calidad que hibridan a distintas
#partes de "housekeeping genes" (genes que se expresan en todo momento en todos los tejidos).
#Idealmente, el ratio entre las distintas regiones del mismo gen no deber�?a diferir de 1 (acá de
#cero). Ratios menores a 3 son aceptables.
ratios(data_qc)

"Normalización"

#Vamos primero al método por RMA
data_rma = rma(data)
data_matrix = exprs(data_rma)

#Ahora normalizamos por GCRMA
data_gcrma = gcrma(data)
gcrma_matrix = exprs(data_gcrma)


#Agregamos los nombres de los genes a las filas de la matriz normalizada
library(RCurl)     # to download data from internet
library(hgu133plus2.db)   # to link AffyIDs to gene names
#library(hgu95av2.db)     # Algunos trabajos usan este otro array
library(plyr)   # to make a dataframe from a function

#Lo primero es añadir el gene symbol a nuestros datos, vinculandolo con el id del spot en el array
# https://www.bioconductor.org/help/workflows/annotation-data/
keytypes(hgu133plus2.db)
#keytypes(hgu95av2.db)

# Necesitamos el apartado SYMBOL
# Mapeamos los AffyIDs a los s�?mbolos
mapAffy_Symbol <- select(hgu133plus2.db, rownames(gcrma_matrix), c("SYMBOL"))

#Como hay filas repetidas, nos quedamos con valores unicos
mapAffy_Symbol = mapAffy_Symbol[!duplicated(mapAffy_Symbol$PROBEID),]

#Ahora usamos la segunda columna de la matriz que acabamos de generar para nombrar las filas de gcrma_matrix
rownames(gcrma_matrix) = mapAffy_Symbol[,2]

#Graficamos los histogramas para los datos normalizados por RMA
sampleNames = vector()
logs = vector()
for (i in 1:14)
{
  sampleNames = c(sampleNames,rep(ph@data[i,1],dim(gcrma_matrix)[1]))
  logs = c(logs,gcrma_matrix[,i])
}

logData = data.frame(logInt=logs,sampleName=sampleNames)

dataHist2 = ggplot(logData, aes(logInt, colour = sampleName)) 
dataHist2 + geom_density()

#Pasamos al boxplot
dataBox = ggplot(logData,aes(sampleName,logInt))
dataBox + geom_boxplot()

#Graficamos los resultados de normalización por GCRMA
sampleNames = vector()
logs = vector()
for (i in 1:14)
{
  sampleNames = c(sampleNames,rep(ph@data[i,1],dim(gcrma_matrix)[1]))
  logs = c(logs,gcrma_matrix[,i])
}

logData = data.frame(logInt=logs,sampleName=sampleNames)

dataHist2 = ggplot(logData, aes(logInt, colour = sampleName)) 
dataHist2 + geom_density()

#Pasamos al boxplot
dataBox = ggplot(logData,aes(sampleName,logInt))
dataBox + geom_boxplot()

#Computamos un PCA de los datos
color=c(rep("blue",10),rep("red",4))
data_PC = prcomp(t(data_matrix),scale=TRUE)
plot(data_PC$x[1:14],col=color)

#Análisis de expresión diferencial
#Primero cargamos una descripción del tratamiento correspondiente a cada archivo .CEL
ph@data[ ,2] = c(rep("FTA",10),rep("Normal",4))
colnames(ph@data)[2]="source"
ph@data
groups = ph@data$source

#Generamos un factor con dos categor�?as (FTA y normal)
f = factor(groups,levels=c("FTA","Normal"))

#Generamos la matriz para correr el ANOVA (T test en este caso con dos niveles)
design = model.matrix(~ 0 + f)
colnames(design) = c("FTA","Normal")

#Ajustamos un modelo lineal a la expresión de cada gen
data_fit = lmFit(gcrma_matrix,design)
data_fit$coefficients[1:10,]

#Una vez que tenemos las expresiones medias, especificamos los contrastes a realizar
contrast.matrix = makeContrasts(FTA-Normal,levels=design)
data_fit_con = contrasts.fit(data.fit,contrast.matrix)
data_fit_con

#Realizamos los t-test ajustados para los contrastes especificados
data_fit_eb = eBayes(data_fit_con)
#Aparece un warning message diciendo que al menos para un gen todos los valores son iguales,
#resultando en una varianza cero. Se lo aparta de la comparación para evitar problemas.
data_fit_eb$p.value[1:10,]
names(data_fit_eb)

#Vemos el fold change para los primeros diez genes
data_fit_eb$coefficients[1:10,]

#Una forma de ver cuántos genes dan diferencialmente expresados con un cierto alpha es:
list_condition <- sapply(data_fit_eb$p.value, function(x) x<0.001)
DE_number  <- length(data_fit_eb$p.value[list_condition])
DE_number

#Sin embargo es necesario ajustar los p-valores para test múltiples!

#Generamos un volcano plot para visualizar la expresión diferencial
name = "Volcano.jpg"
jpeg(name)
volcanoplot(data_fit_eb,coef=1,highlight=20)
dev.off()

#Llegando al final, generamos una lista de genes diferencialmente expresados en orden
#de significancia.
tab = topTable(data_fit_eb,coef=1,number=1000,adjust.method='BH')
tab

#Ahora s�?, podemos ver cuántos genes se expresan diferencialmente para un cierto alpha
topgenes = tab[tab[, "adj.P.Val"] < 0.05, ]
dim(topgenes)
topgenes

#Generamos entonces la lista de arriba con todos los genes que se expresan diferencialmente
tab = topTable(data_fit_eb,coef=1,number=dim(topgenes)[1],adjust.method='BH')
tab
dim(tab)

#Vemos ahora cuáles aparecen sobreexpresados y cuáles subexpresados (filtramos por significancia
#biológica)
topups = topgenes[topgenes[, "logFC"] > 1, ]
dim(topups)
topdowns = topgenes[topgenes[, "logFC"] < -1, ]
dim(topdowns)

topups
