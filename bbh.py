import sys, os, re
import pandas as pd

script, db1, db2 = sys.argv

#Indexes both databases
os.system("makeblastdb -in {} -input_type fasta -dbtype prot".format(db1))
os.system("makeblastdb -in {} -input_type fasta -dbtype prot".format(db2))

#Run Blast in both directions
if not os.path.isfile("blast_dir1"):
    os.system("blastp -db {} -query {} -evalue 1e-5 -outfmt 6 -qcov_hsp_perc 70 -out {}".format(db1,db2,"blast_dir1"))
if not os.path.isfile("blast_dir2"):
    os.system("blastp -db {} -query {} -evalue 1e-5 -outfmt 6 -qcov_hsp_perc 70 -out {}".format(db2,db1,"blast_dir2"))

#Import matching library
sys.path.append('/mnt/c/Users/l_vey/Documents/Estudio/Labo/Repositorios/Eze/sndg-bio')

from SNDG.Sequence.BBH import BBH

#Save matching orthologs to a file
bbhs = BBH.bbhs_from_blast("blast_dir1","blast_dir2")

print("Human genes taken into account: "+str(len(set([line[1] for line in bbhs]))))

#Save a list with capitalised names of all query bbhs
query_bbhs = set([line[0] for line in bbhs])
gene_list = [re.findall("\|.*\|(.*)_RAT",line)[0].capitalize() for line in query_bbhs]

#Load a .csv file containing all genes present in the array of interest
Rat_array = pd.read_csv("Rat_genes.csv")
Array_genes = [gene for gene in Rat_array["SYMBOL"]]

#Let's save all bbhs that are annotated in the array
Intersection = set(gene_list).intersection(Array_genes)

print(len(gene_list))

print("Rat orthologs found: "+str(len(Intersection)))

#Saves the new genelist
with open("Genelist_rat.txt", 'w') as x:
    x.write("GENES\n")
    for i in Intersection:
        x.write(i+"\n")
