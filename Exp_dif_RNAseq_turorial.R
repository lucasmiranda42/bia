#Primero instalamos los paquetes necesarios
install.packages("dplyr")
install.packages("devtools")
source("https://www.bioconductor.org/biocLite.R")
biocLite(c("alyssafrazee/RSkittleBrewer", "ballgown", "genefilter"))

#Cargamos ahora los paquetes a utilizar
library(ballgown)
library(RSkittleBrewer)
library(genefilter)
library(dplyr)
library(devtools)
library(limma)

#Creamos una variable con el directorio a la carpeta que tiene todas las salidas de StringTie
data_directory = "/home/lmiranda/Escritorio/Tutorial_Romi/chrX_data/Alineamientos"
setwd(data_directory)
data_directory

#Posteriormente, creamos un ballgown object. Es importante que la estructura de las carpetas conteniendo
#los datos se mantenga: Una carpeta root con una subcarpeta por cada sample a analizar, conteniendo solo
#los archivos .ctab correspondientes (tienen que ser 5).
bg = ballgown(dataDir=data_directory, samplePattern='sample', meas='all')

#bg es ahora una ballgown instance, que contiene 34102 transcriptos y 2 muestras
bg

#sobre este objeto podemos ejecutar un montón de métodos
methods(class='ballgown')

#por ejemplo, podemos obtener los niveles de expresión de genes, transcriptos, exones e intrones usando los
#siguientes comandos (por defecto los valores son retornados en FKPM).
head(gexpr(bg),5)
head(texpr(bg),5)
head(eexpr(bg),5)
head(iexpr(bg),5)

#La función que sigue grafica los transcriptos para el gen especificado en las dos muestras.
plotTranscripts('NM_000033', bg, 
                samples=c('sample01', 'sample02'), 
                meas='FPKM', colorby='transcript')

#Lo siguiente es filtrar los transcriptos con baja varianza. Quedándonos sólo con aquellos genes que varían más del
#cutoff. rowVars es una función del paquete genefilter que calcula la varianza por fila
bg_filt <- subset(bg, "rowVars(texpr(bg)) >1", genomesubset=TRUE)
bg_filt



#Corremos el análisis de expresión diferencial propiamente dicho, creando primero una variable que contenga la info
#fenot�pica de las muestras (a qué condición corresponde cada una, en nuestro caso S1 y S2)
pData(bg_filt) = data.frame(id=sampleNames(bg_filt), group=rep(c("A","B"), each=6))
pData(bg_filt)

#Creamos la matriz con los valores de expresión en FPKM por transcrito
texpVals = texpr(bg_filt)
rownames(texpVals) = geneNames(bg_filt)

gexpVals = aggregate(texpVals ~ rownames(texpVals), data=texpVals, sum)
rows <- gexpVals[,1]
gexpVals = gexpVals[,-1]
rownames(gexpVals) <- rows

############################# HASTA AC� GENERAMOS LA MATRIZ DE EXPRESI�N ############################ 

#Corremos entonces el an�lisis de expresi�n diferencial
stat_results = stattest(bg_filt, feature='transcript', meas='FPKM', covariate='group', getFC = TRUE)
head(stat_results)

#El resultado del test no tiene ningún identificador para los genes. Podemos agregarlo
stat_results <- data.frame(geneNames = geneNames(bg_filt),
                                  geneIDs = geneIDs(bg_filt),
                                  stat_results)

#Ordenamos ahora los resultados por p-value creciente, quedando los resultados más significativos
#arriba.
stat_results = arrange(stat_results,qval)
stat_results

#Guardamos los resultados a un .csv que pueda ser analizado y distribuido posteriormente
write.csv(stat_results, "chrX_transcript_results.csv", row.names=FALSE)

#Creamos un subset con aquellos transcriptos con p-value menor a 0.05 (o el alpha elegido)
DE_list = subset(stat_results,stat_results$qval<0.20)
DE_genes = as.character(DE_list$geneNames)

#Por último podemos visualizar los datos generados con algunas figuras
library(ggplot2)
library(cowplot)

stat_results$mean <- rowMeans(texpr(bg_filt))

ggplot(stat_results, aes(log2(mean), log2(fc), color = qval<0.05)) +
  scale_color_manual(values=c("#999999", "#FF0000")) +
  geom_point(size=0.5) +
  geom_hline(yintercept=0)


################################          HEATMAPS          #########################################   

library(heatmap3)

#Nos quedamos sólo con las filas de la matriz que corresponden a genes diferencialmente expresados

DE_table = gexpVals[rownames(gexpVals) %in% DE_genes,]

#Reordenamos las filas por fold change
datac        = data.matrix(DE_table)
distancec  = dist(datac, method="manhattan")
clusterc     = hclust(distancec, method="ward.D")
dendrogramc  = as.dendrogram(clusterc)
Rowvc        = rowMeans(datac, na.rm = T)
dendrogramc  = reorder(dendrogramc, Rowvc)

#Si da error el heatmap, ejecutar esto:
dev.off()

# Graficamos con heatmap3
par(mar=c(1,1,1,1))
heatmap3(DE_table,
         Rowv = dendrogramc,
         cex.main = 2,
         cexRow = 1,
         cexCol = 1,
         margins = c(20, 1),
         ColSideColors = rep(c("red","blue"),each=6))

#Ayuda a localizar los puntos en los cuales colocar los segmentos (y nombres)
pts <- locator(4)

#Agregamos etiquetas y tìtulos al gráfico
title( main = "Comparación tiroides normales versus ATC",
       cex.main = 0.8, adj=(pts$x[1]+pts$x[4])/2,pts$y[1])

# Agregamos etiquetas para los tratamientos
# https://stat.ethz.ch/R-manual/R-devel/library/graphics/html/text.html
text((pts$x[1]+pts$x[2])/2,pts$y[1]+0.03, "PTC", cex = 1.2)
text((pts$x[3]+pts$x[4])/2,pts$y[1]+0.03, "Normal", cex = 1.2)


