#Primero instalamos los paquetes necesarios
source("https://www.bioconductor.org/biocLite.R")
biocLite(c("alyssafrazee/RSkittleBrewer", "ballgown", "genefilter", "Biobase","DESeq2"))
#Cargamos ahora los paquetes a utilizar
library(ballgown)
library(RSkittleBrewer)
library(genefilter)
library(dplyr)
library(devtools)
library(reshape2)
library(Biobase)
library(DESeq2)
library(ggplot2)
library(cowplot)
library(heatmap3)
library(BiocGenerics)
library(mixOmics)
library(edgeR)
#                                                       CARGA DE DATOS                                           #


#Creamos una variable con el directorio a la carpeta que tiene todas las salidas de StringTie
data_directory = "C:\\Users\\l_vey\\Documents\\Estudio\\Labo\\Colaboraciones\\Juan_Nicola\\RNAseq\\PRJEB11591"

#Posteriormente, creamos un ballgown object. Es importante que la estructura de las carpetas conteniendo
#los datos se mantenga: Una carpeta root con una subcarpeta por cada sample a analizar, conteniendo solo
#los archivos .ctab correspondientes (tienen que ser 5).
bg = ballgown(dataDir=data_directory, samplePattern='ERR', meas='all')

#Corremos el análisis de expresión diferencial propiamente dicho, creando primero una variable que contenga la info
#fenotípica de las muestras (a qué condición corresponde cada una, en nuestro caso S1 y S2)
conditions = c("FA","FTC","FV","PTC")
pData(bg) = data.frame(id=sampleNames(bg), group=c(rep("FA",14),rep("FTC",21),rep("FV",14),rep("PTC",10)))
pData(bg)

#Definimos la lista de genes de interés
nfkb_genes = read.table("Genelist1.txt", header = TRUE)$GENES
nfkb_genes = as.vector(nfkb_genes)
nfkb_genes = unique(nfkb_genes)

#Lo siguiente es filtrar los transcriptos con baja varianza. Quedándonos sólo con aquellos genes que varían más del
#cutoff. rowVars es una función del paquete genefilter que calcula la varianza por fila
bg_filt = subset(bg, "rowVars(texpr(bg)) > 1", genomesubset=TRUE)
rm(bg)

#Seguimos sólo (por ahora) con el contraste FA_PTC. Una vez que tenga establecido el pipeline,
#voy a crear una función que analice todo junto.


#Filtramos ahora por tratamiento
FA_PTC_filt = subset(bg_filt, "group == 'FA' | group == 'PTC'", genomesubset=FALSE)

#Filtro los genes NFKB (el testeo tiene que ser independiente, ya que si no sobrepenalizamos los p-values)
FA_PTC_NFKB_filt = subset(FA_PTC_filt, "gene_name %in% nfkb_genes")


#                                                    CONTROL DE CALIDAD                                          #


#Pasamos los fkpm de genes a un data frame
bg_df_wide = as.data.frame(texpr(FA_PTC_filt))
bg_df = melt(bg_df_wide, variable_name = "Samples")
bg_df = data.frame(bg_df, Condition = c(rep("FA",14*dim(bg_df_wide)[1]),rep("PTC",10*dim(bg_df_wide)[1])))

#Box plot
ggplot(bg_df, aes(x = Condition, y = log2(value+1), fill = Condition)) + geom_boxplot() + xlab("") +
  ylab(expression(log[2](FKPM))) + scale_fill_manual(values = c("#619CFF","#F564E3","#61ffb7","#61e2ff"))

#Distribuciones de densidad
ggplot(bg_df, aes(x = log2(value+1), colour = Condition, fill = variable)) + ylim(c(0, 0.55)) +
  geom_density(alpha = 0.05, size = 1.25) + xlab(expression(log[2](FKPM))) + theme(legend.position="none")


#                                              NORMALIZACIÓN ENTRE MUESTRAS                                      #


#PCA para ver si los datos se agrupan según condición
pca = prcomp(t(bg_df_wide))

pca_data_perc=round(100*pca$sdev^2/sum(pca$sdev^2),1)
df_pca_data = data.frame(PC1 = pca$x[,1], PC2 = pca$x[,2], sample = colnames(bg_df_wide), group=c(rep("FA",14),rep("PTC",10)))

library(ggrepel)
ggplot(df_pca_data, aes(PC1,PC2, color = group))+
  geom_point(size=8)+
  labs(x=paste0("PC1 (",pca_data_perc[1],")"), y=paste0("PC2 (",pca_data_perc[2],")"))  + theme(legend.position="top")

#Análisis de batch effect
mat.dist = bg_df_wide
colnames(mat.dist) = c(rep("FA",14),rep("PTC",10)) 
mat.dist = as.matrix(dist(t(mat.dist))) 
mat.dist = mat.dist/max(mat.dist)
cim(mat.dist, symkey = FALSE, margins = c(9, 9))



#                                                  CONTROL DE CALIDAD NFKB                                       #


#Pasamos los fkpm de genes a un data frame
nfkb_df_wide = as.data.frame(texpr(FA_PTC_NFKB_filt))
nfkb_df = melt(nfkb_df_wide, variable_name = "Samples")
nfkb_df = data.frame(nfkb_df, Condition = c(rep("FA",14*dim(nfkb_df_wide)[1]),rep("PTC",10*dim(nfkb_df_wide)[1])))

#Box plot
ggplot(nfkb_df, aes(x = Condition, y = log2(value+1), fill = Condition)) + geom_boxplot() + xlab("") +
  ylab(expression(log[2](FKPM))) + scale_fill_manual(values = c("#619CFF","#61e2ff"))

#Distribuciones de densidad
ggplot(nfkb_df, aes(x = log2(value+1), colour = Condition, fill = variable)) + ylim(c(0, 0.45)) +
  geom_density(alpha = 0.05, size = 1.25) + xlab(expression(log[2](FKPM))) + theme(legend.position="none")


# 


#Dejo un comando para borrar el objeto original y que no ocupe memoria
rm(bg_filt)


#                                                   EXPRESIÓN DIFERENCIAL                                        #


#Corremos entonces todos los análisis de expresión diferencial
FA_PTC_results = stattest(FA_PTC_filt, feature='transcript', meas='FPKM', covariate='group', getFC = TRUE)

#Expresión diferencial para los transcripts NFKB
FA_PTC_NFKB = stattest(FA_FTC_NFKB_filt, feature='transcript', meas='FPKM', covariate='group', getFC = TRUE)

#Los resultados de los test no tiene ningún identificador para los transcripts. Podemos agregarlos
FA_PTC_results <- data.frame(geneNames = geneNames(FA_PTC_filt),geneIDs = geneIDs(FA_PTC_filt),FA_PTC_results)
FA_PTC_NFKB <- data.frame(geneNames = geneNames(FA_PTC_NFKB_filt),geneIDs = geneIDs(FA_PTC_NFKB_filt),FA_PTC_NFKB)

#Ordenamos ahora los resultados por p-value creciente, quedando los resultados más significativos
#arriba.
FA_PTC_results = arrange(FA_PTC_results,qval)
FA_PTC_NFKB = arrange(FA_PTC_NFKB,qval)

#Guardamos los resultados a un .csv que pueda ser analizado y distribuido posteriormente
write.csv(FA_PTC_results, "FA_PTC_results.csv", row.names=FALSE)

#Guardamos en .CSV los archivos con la ED de los genes NFKB
write.csv(FA_PTC_NFKB, "FA_PTC_NFKB.csv", row.names=FALSE)
















#Por ahora voy a seguir trabajando solo con los genes NFKB. Despues retomo el resto

#Sigo sólo con el perfil global de FA_PTC (el que debería mostrar más contraste).
FA_PTC_DE = as.character(subset(FA_PTC_results,FA_PTC_results$qval<0.05)[,1])

#Filtro el objeto bg con los genes diferencialmente expresados
FA_PTC_filt_DE = subset(FA_PTC_filt, "gene_name %in% FA_PTC_DE")

#Pasamos los fkpm de genes a un data frame
fa_ptc_df_wide = as.data.frame(texpr(FA_PTC_filt_DE))

#PCA para ver si los datos se agrupan según condición
pca = prcomp(t(fa_ptc_df_wide))

pca_data_perc=round(100*pca$sdev^2/sum(pca$sdev^2),1)
df_pca_data = data.frame(PC1 = pca$x[,1], PC2 = pca$x[,2], sample = colnames(fa_ptc_df_wide), group=c(rep("FA",14),rep("PTC",10)))

library(ggrepel)
ggplot(df_pca_data, aes(PC1,PC2, color = group))+
  geom_point(size=8)+
  labs(x=paste0("PC1 (",pca_data_perc[1],")"), y=paste0("PC2 (",pca_data_perc[2],")"))  + theme(legend.position="top")

#Vamos ahora sí a generar los heatmaps!

#Extraemos las matrices de cada set
FA_PTC_matrix = gexpr(FA_PTC_filt_DE) 

#Agrupamos por clusters
#1
datac        = data.matrix(FA_PTC_matrix)
distancec  = dist(datac)
clusterc     = hclust(distancec, method="average")
dendrogramc  = as.dendrogram(clusterc)
Rowvc        = rowMeans(datac, na.rm = T)
dendrogramc  = reorder(dendrogramc, Rowvc)

#Si da error el heatmap, ejecutar esto:
dev.off()

# Graficamos con heatmap3
par(mar=c(1,1,1,1))
heatmap3(FA_PTC_matrix,
         Colv = NA,
         Rowv = dendrogramc, 
         cex.main = 2,
         cexRow = 1,
         cexCol = 1)
