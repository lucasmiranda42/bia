import argparse, os, subprocess, re, time
import numpy as np
from multiprocessing import Pool, cpu_count

def RNAseq_pipeline(sample_type, read_files, index, filters, annotation):
    """Executes a full RNAseq pipeline in the given paired-end reads, including quality filtering, mapping and transcriptome assembly."""

    #Parsing of read files, regarding sample sequencing type
    if sample_type == 'SINGLE':
        read_file = read_files
    elif sample_type == 'PAIRED':
        read_file1, read_file2 = read_files[0], read_files[1]

    #Prinseq FastQfiltering
    print("Started prinseq filtering...")

    start = time.time()
    if sample_type == 'SINGLE':
        p = subprocess.call("prinseq-lite.pl -fastq {} -out_format 3 {} -out_good trimmed".format(read_file, filters), shell=True)
    elif sample_type == 'PAIRED':
        p = subprocess.call("prinseq-lite.pl -fastq {} -fastq2 {} -out_format 3 {} -out_good trimmed".format(read_file1, read_file2, filters), shell=True)

    delay_filt = np.round(time.time()-start, 2)

    if p==0: print("Filtering complete in {} seconds!".format(str(delay_filt)))
    else: pass

    #Delete bad sequence files (I delete this here because, unlike all files deleted below, it won't be usea AT ALL)
    #print("Cleaning filtered sequences...")

    #p = subprocess.call("rm *bad*", shell=True)

    #if p==0: print("Cleaning complete!")
    #else: pass

    #HISAT2 mapping
    print("Started mapping with HISAT2...")

    start = time.time()

    if sample_type == 'SINGLE':
        p = subprocess.call("hisat2 -x {} -U trimmed.fastq -S HISAT_out.sam".format(index), shell=True)
    elif sample_type == 'PAIRED':
        p = subprocess.call("hisat2 -x {} -1 trimmed_1.fastq -2 trimmed_2.fastq -S HISAT_out.sam".format(index), shell=True)

    delay_map = np.round(time.time()-start, 2)

    if p==0: print("Mapping complete in {} seconds!".format(str(delay_map)))
    else: pass

    #samtools .sam to .bam conversion
    print("Started sam to bam conversion using samtools...")

    start = time.time()

    p = subprocess.call("samtools view -Su HISAT_out.sam | samtools sort -o alns.sorted.bam -", shell=True)

    delay_conv = np.round(time.time()-start, 2)

    if p==0: print("SAM to BAM conversion complete in {} seconds!".format(str(delay_conv)))
    else: pass

    #Transcriptome assembly with StringTie
    print("Started transcriptome assembly using stringtie...")

    start = time.time()

    p = subprocess.call("stringtie alns.sorted.bam -o main_output.gtf -f 0.15 -A gene_abund.tab -C cov_refs.gtf -G {} -B -e".format(annotation), shell=True)

    delay_trans = np.round(time.time()-start)

    if p==0: print("Transcriptome assembly complete in {} seconds!".format(str(delay_trans)))
    else: pass

    #Removes the .sam file that was still around
    subprocess.call("rm *.sam", shell=True)


def Run_pipeline(read_list):
    """Runs an RNAseq pipeline sequentially in a specified list of paired-end reads"""

    Error_log = []

    #Now we run the pipeline for each pair in read_list
    if sample_type == 'SINGLE':
        for reads in read_list:
            subprocess.call("gunzip {}".format(reads), shell=True)

            #Creates a subfolder for each input file
            subprocess.call('mkdir {}'.format(reads[:-9]), shell=True)
            os.chdir(reads[:-9])
            #[!!!] Main step of this function! executes the RNAseq pipeline
            RNAseq_pipeline(sample_type, reads[:-3], index, filters, annotation)
            os.chdir(owd)

            #Saves Error_log to a file
            with open("Error_log.txt", "a") as x:
                for error in Error_log:
                    x.write(error)
                    x.write("\n")

    elif sample_type == 'PAIRED' :
        #read_list = [reads.split(';') for reads in read_list]

        for reads in read_list:
            subprocess.call("gunzip {}".format(reads[0]), shell=True)
            subprocess.call("gunzip {}".format(reads[1]), shell=True)

            reads = [read[:-3] for read in reads]

            #Creates a subfolder for each input file
            subprocess.call('mkdir {}'.format(reads[0][:-8]), shell=True)
            os.chdir(reads[0][:-8])
            #[!!!] Main step of this function! executes the RNAseq pipeline
            RNAseq_pipeline(sample_type, reads, index, filters, annotation)
            os.chdir(owd)

            #Saves Error_log to a file
            with open("Error_log.txt", "a") as x:
                for error in Error_log:
                    x.write(error)
                    x.write("\n")

def split(a, n):
    """Splits a list into n roughly equal parts"""
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Runs an RNAseq pipeline for all .fastq paired-end reads in the specified file')
    parser.add_argument('--file_in', '-n', type=str, help='Input file containing the names of all reads')
    parser.add_argument('--filters', '-f', type=str, help='List of filters for Prinseq, separated by commas and begining with +')
    parser.add_argument('--sample_type', '-t', type=str, help='Specify whether you are dealing with SINGLE or PAIRED end data')
    parser.add_argument('--index', '-i', type=str, help='ABSOLUTE PATH of the Index of reference genome to take into account when mapping')
    parser.add_argument('--annotation', '-a', type=str, help='ABSOLUTE PATH of the .gtf annotation of all genes in the given reference. This will be used in the transcriptome assembly step')
    parser.add_argument('--processors', '-p', type=int, default=cpu_count(), help='Number of processors for paralellization')

    args = parser.parse_args()
    file_in = args.file_in
    filters = args.filters
    sample_type = args.sample_type
    index = args.index
    annotation = args.annotation
    processors = args.processors

    #Parses prinseq-lite filters
    filters = ' '.join(filters.replace('+','-').split(','))

    #Opens and parses the inputfile. Stores all experiments in a list
    with open(file_in) as f:
        data = [line.replace('\n','') for line in f]

    if sample_type == 'SINGLE': read_list = data
    elif sample_type == 'PAIRED': read_list = [i.split(';') for i in data]

    #Let's save the working directory
    owd = os.getcwd()

    #Now we want to split out read_list between processors!
    reads_split = list(split(read_list, processors))

    #Finally, let's parallelize and run the functions!
    with Pool(processors) as p:
        p.map(Run_pipeline, reads_split)
